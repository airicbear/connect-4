# Connect 4

Connect 4 in C.

## Objective

Make a Connect 4 game using the C programming language.

## Motivation

After playing Connect 4 in the dorm lobby with Nathan, it inspired me to write a C program for Connect 4 since I was teaching him C in the morning.
